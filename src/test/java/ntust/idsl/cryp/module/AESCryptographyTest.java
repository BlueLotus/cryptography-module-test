package ntust.idsl.cryp.module;

import static org.junit.Assert.*;
import ntust.idsl.cryp.module.aes.AESDecryptor;
import ntust.idsl.cryp.module.aes.AESEncryptor;
import ntust.idsl.cryp.module.aes.AESKeyGenerator;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Carl Adler (C.A.)
 * */
public class AESCryptographyTest {
	
	private static byte[] secretKey;
	private static String initialVector;
	
	@BeforeClass
	public static void before() {
		System.out.println("Test material initializing...\n");
		secretKey = AESKeyGenerator.generateSecretKey();
		initialVector = "0102030405060708";
	}
	
	@Test
	public void cryptoStringWithAESECBMode() {
		System.out.println("---AES ECB Mode Start---");
		String strToEncrypt = "Carl Adler";
		System.out.println("String to be encrypted: " + strToEncrypt);
		AESKeyGenerator.writeKeyToFile(secretKey);
		System.out.println("Secret Key: " + new String(secretKey));
		String strToDecrypt = AESEncryptor.encryptForStringWithECBMode(strToEncrypt, secretKey);
		System.out.println("String after encrypted: " + strToDecrypt);
		String decryptedString = AESDecryptor.decryptForStringWithECBMode(strToDecrypt, AESKeyGenerator.obtainKeyFromFile());
		System.out.println("String after decrypted: " + decryptedString);
		assertEquals(strToEncrypt, decryptedString);
		System.out.println("---AES ECB Mode End---\n");
	}
	
	@Test
	public void cryptoStringWithAESCBCMode() {
		System.out.println("---AES CBC Mode Start---");
		String strToEncrypt = "Miffy Adler";
		System.out.println("String to be encrypted: " + strToEncrypt);
		AESKeyGenerator.writeKeyToFile(secretKey);
		System.out.println("Secret Key: " + new String(secretKey));
		String strToDecrypt = AESEncryptor.encryptForStringWithCBCMode(strToEncrypt, secretKey, initialVector);
		System.out.println("String after encrypted: " + strToDecrypt);
		String decryptedString = AESDecryptor.decryptForStringWithCBCMode(strToDecrypt, AESKeyGenerator.obtainKeyFromFile(), initialVector);
		System.out.println("String after decrypted: " + decryptedString);
		assertEquals(strToEncrypt, decryptedString);
		System.out.println("---AES CBC Mode End---\n");
	}
	
	@Test
	public void keyIOTest() {
		System.out.println("---Key IO functionality test---");
		AESKeyGenerator.writeKeyToFile(secretKey);
		System.out.println("Secret Key: " + new String(secretKey));
		byte[] obtainedKey = AESKeyGenerator.obtainKeyFromFile();
		System.out.println("Obtained Key: " + new String(obtainedKey));
		assertEquals(new String(secretKey), new String(obtainedKey));
		System.out.println("---Key IO functionality test complete---");
	}

}
