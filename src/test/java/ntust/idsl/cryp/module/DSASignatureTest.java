package ntust.idsl.cryp.module;

import static org.junit.Assert.*;

import java.security.Signature;

import ntust.idsl.cryp.module.dsa.DSAKeyGenerator;
import ntust.idsl.cryp.module.dsa.DSASignatureCrossComparator;
import ntust.idsl.cryp.module.dsa.DSASignatureSigner;
import ntust.idsl.cryp.module.dsa.DSASignatureVerifier;
import ntust.idsl.cryp.util.Order;
import ntust.idsl.cryp.util.OrderedRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Carl Adler (C.A.)
 * */
@RunWith(OrderedRunner.class)
public class DSASignatureTest {

	Signature signature;
	String message;
	
	@Before
	public void initialize() {
		try {
			signature = Signature.getInstance("DSA");
			message = "Miffy Adler";
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@Order(order = 1)
	public void DSASignatureVerificationProcessForClientSide() {
		DSAKeyGenerator.generateDSAKeyPair("client");
		DSASignatureSigner.signSignatureFile(signature, message, "client");
		assertTrue(DSASignatureVerifier.verifySignatureFile(signature, message, "client"));
	}
	
	@Test
	@Order(order = 2)
	public void DSASignatureVerificationProcessForServerSide() {
		DSAKeyGenerator.generateDSAKeyPair("server");
		DSASignatureSigner.signSignatureFile(signature, message, "server");
		assertTrue(DSASignatureVerifier.verifySignatureFile(signature, message, "server"));
	}
	
	@Test
	@Order(order = 3)
	public void crossCompareSignature() {
		assertFalse(DSASignatureCrossComparator.crossCompare(signature, message));
	}

}
