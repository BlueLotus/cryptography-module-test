package ntust.idsl.cryp.module;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AESCryptographyTest.class, CipherIntegrationTest.class,
		DSASignatureTest.class })
public class ModuleTest {

}
