package ntust.idsl.cryp.module;

import static org.junit.Assert.*;

import java.security.Signature;

import ntust.idsl.cryp.module.aes.AESDecryptor;
import ntust.idsl.cryp.module.aes.AESEncryptor;
import ntust.idsl.cryp.module.aes.AESKeyGenerator;
import ntust.idsl.cryp.module.dsa.DSASignatureSigner;
import ntust.idsl.cryp.module.dsa.DSASignatureVerifier;
import ntust.idsl.cryp.util.Order;
import ntust.idsl.cryp.util.OrderedRunner;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Carl Adler (C.A.)
 * */
@RunWith(OrderedRunner.class)
public class CipherIntegrationTest {
	
	private static byte[] secretKey;
	private static String initialVector;
	private static String strToEncrypt = "Miffy Adler";
	private static Signature signature;
	
	@BeforeClass
	public static void before() {
		try {
			System.out.println("Test material initializing...\n");
			secretKey = AESKeyGenerator.obtainKeyFromFile();
			initialVector = "3102843462069914";
			signature = Signature.getInstance("DSA");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Order(order = 1)
	public void AESCBCModeWithDSASignatureFileProcess() {
		System.out.println("---AES CBC Mode With DSA Signature File Process Started---\n");
		
		//For request sender side.
		System.out.println("	String to be encrypted: " + strToEncrypt);
		System.out.println("	Secret Key: " + new String(secretKey));
		String strToDecrypt = AESEncryptor.encryptForStringWithCBCMode(strToEncrypt, secretKey, initialVector);
		System.out.println("	String after encrypted: " + strToDecrypt + "\n");
		DSASignatureSigner.signSignatureFile(signature, strToDecrypt, "client");
		
		//For request receiver side.
		assertTrue(DSASignatureVerifier.verifySignatureFile(signature, strToDecrypt, "client"));
		String decryptedString = AESDecryptor.decryptForStringWithCBCMode(strToDecrypt, secretKey, initialVector);
		System.out.println("	String after decrypted: " + decryptedString);
		assertEquals(strToEncrypt, decryptedString);
		
		System.out.println("\n---AES CBC Mode With DSA Signature File Process End---");
	}
	
	@Test
	@Order(order = 2)
	public void AESCBCModeWithDSASignatureMessageProcess() {
		System.out.println("---AES CBC Mode With DSA Signature Message Process Started---\n");
		System.out.println("	String to be encrypted: " + strToEncrypt);
		System.out.println("	Secret Key: " + new String(secretKey));
		String encryptedMessage = AESEncryptor.encryptForStringWithCBCMode(strToEncrypt, secretKey, initialVector);
		System.out.println("	String after encrypted: " + encryptedMessage + "\n");
		String signedMessage = DSASignatureSigner.signSignatureMessage(signature, encryptedMessage, "client");
		
		assertTrue(DSASignatureVerifier.verifySignedMessage(signature, signedMessage, encryptedMessage, "client"));
		String decryptedString = AESDecryptor.decryptForStringWithCBCMode(encryptedMessage, secretKey, initialVector);
		System.out.println("	String after decrypted: " + decryptedString);
		assertEquals(strToEncrypt, decryptedString);
		System.out.println("\n---AES CBC Mode With DSA Signature Message Process End---");
	}

}
