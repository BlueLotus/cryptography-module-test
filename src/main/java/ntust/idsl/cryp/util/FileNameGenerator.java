package ntust.idsl.cryp.util;

/**
 * @author Carl Adler (C.A.)
 * */
public class FileNameGenerator {
	
	private static String location  = System.getProperty("user.dir");
	public static String PRIVATE_KEY_FOR_CLIENT = location + "\\privateKeyForClient.key";
	public static String PRIVATE_KEY_FOR_SERVER = location + "\\privateKeyForServer.key";
	public static String PUBLIC_KEY_FOR_CLIENT = location + "\\publicKeyForClient.key";
	public static String PUBLIC_KEY_FOR_SERVER = location + "\\publicKeyForServer.key";
	public static String SIGNATRUE_FOR_CLIENT = location + "\\signatureForClient.sig";
	public static String SIGNATURE_FOR_SERVER = location + "\\signatureForServer.sig";

}
