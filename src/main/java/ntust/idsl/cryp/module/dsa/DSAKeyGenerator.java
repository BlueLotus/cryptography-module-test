package ntust.idsl.cryp.module.dsa;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import ntust.idsl.cryp.util.FileNameGenerator;

/**
 * @author Carl Adler (C.A.)
 * */
public class DSAKeyGenerator {
	
	private static KeyPairGenerator generator;
	private static KeyPair keyPair;
	private static PrivateKey privateKey;
	private static PublicKey publicKey;
	
	public static void generateDSAKeyPair(String party) {
		try {
			generateKeyPair(party);
			generateKeyFile(party);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void generateKeyPair(String party) throws Exception {
		System.out.printf("Generating key set for %s side...\n", party);
		generator = KeyPairGenerator.getInstance("DSA");
		generator.initialize(1024, new SecureRandom());
		keyPair = generator.generateKeyPair();
		privateKey = keyPair.getPrivate();
		publicKey = keyPair.getPublic();
		System.out.println("Private Key: " + privateKey.toString());
		System.out.println("Public Key: " + publicKey.toString());
	}
	
	private static void generateKeyFile(String party) throws Exception {
		ObjectOutputStream osPrivate = null;
		ObjectOutputStream osPublic = null;
		if(party.equalsIgnoreCase("client")) {
			osPrivate = new ObjectOutputStream(new FileOutputStream(FileNameGenerator.PRIVATE_KEY_FOR_CLIENT));
			osPublic = new ObjectOutputStream(new FileOutputStream(FileNameGenerator.PUBLIC_KEY_FOR_CLIENT));
		} else if(party.equalsIgnoreCase("server")) {
			osPrivate = new ObjectOutputStream(new FileOutputStream(FileNameGenerator.PRIVATE_KEY_FOR_SERVER));
			osPublic = new ObjectOutputStream(new FileOutputStream(FileNameGenerator.PUBLIC_KEY_FOR_SERVER));
		} else {
			throw new Exception("Party name mismatched. \n");
		}
		osPrivate.writeObject(privateKey);
		osPublic.writeObject(publicKey);
		osPrivate.close();
		osPublic.close();
	}
}
