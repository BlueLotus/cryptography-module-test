package ntust.idsl.cryp.module.dsa;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.PublicKey;
import java.security.Signature;

import ntust.idsl.cryp.util.FileNameGenerator;

/**
 * @author Carl Adler (C.A.)
 * */
public class DSASignatureCrossComparator {
	
	private static FileInputStream fileInputStream;
	private static ObjectInputStream objInputStream;
	private static PublicKey publicKey;
	private static boolean result = false;
	
	public static boolean crossCompare(Signature signature, String message) {
		System.out.println("--- Cross Compare Process Started ---");
		try {
			initializeFileResources("client");
			boolean result1 = verifySignatureWithMessage(signature, message, "client", "server");
			initializeFileResources("server");
			boolean result2 = verifySignatureWithMessage(signature, message, "server", "client");
			if(result1 && result2)
				result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("--- Cross Compare Process End ---");
		return result;
	}
	
	private static void initializeFileResources(String party) throws Exception {
		if(party.equalsIgnoreCase("client")) {
			fileInputStream = new FileInputStream(FileNameGenerator.SIGNATRUE_FOR_CLIENT);
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PUBLIC_KEY_FOR_SERVER));
		} else if(party.equalsIgnoreCase("server")) {
			fileInputStream = new FileInputStream(FileNameGenerator.SIGNATURE_FOR_SERVER);
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PUBLIC_KEY_FOR_CLIENT));
		} else {
			throw new Exception("Party name mismatched. \n");
		}
		publicKey = (PublicKey) objInputStream.readObject();
		objInputStream.close();
	}
	
	private static boolean verifySignatureWithMessage(Signature signature, String message, String party1, String party2) throws Exception {
		System.out.printf("\nVerify the signatrue of %s with the public key of %s...\n", party1, party2);
		boolean verification = false;
		signature.initVerify(publicKey);
		signature.update(message.getBytes());
		byte[] raw = new byte[fileInputStream.available()];
		fileInputStream.read(raw);
		fileInputStream.close();
		System.out.println("Original Message: " + message);
		System.out.println("Signed message needs to verified: " + new String(raw));
		if(signature.verify(raw)) {
			System.out.println("Verify signature successfully.");
			verification = true;
		} else {
			System.out.println("Verify signature failed.\n");
		}
		return verification;
	}
}
