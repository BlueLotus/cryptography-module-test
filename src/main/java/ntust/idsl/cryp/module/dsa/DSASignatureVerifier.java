package ntust.idsl.cryp.module.dsa;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.PublicKey;
import java.security.Signature;

import org.apache.commons.codec.binary.Base64;

import ntust.idsl.cryp.util.FileNameGenerator;

/**
 * @author Carl Adler (C.A.)
 * */
public class DSASignatureVerifier {
	
	private static FileInputStream fileInputStream;
	private static ObjectInputStream objInputStream;
	private static PublicKey publicKey;
	private static boolean result = false;
	
	public static boolean verifySignatureFile(Signature signature, String message, String party) {
		System.out.println("	--- Verify Signature File Process Started ---");
		try {
			initializeFileResources(party);
			verifySignatureWithMessage(signature, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("	--- Verify Signature File Process Finished ---\n");
		return result;
	}
	
	private static void initializeFileResources(String party) throws Exception {
		if(party.equalsIgnoreCase("client")) {
			fileInputStream = new FileInputStream(FileNameGenerator.SIGNATRUE_FOR_CLIENT);
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PUBLIC_KEY_FOR_CLIENT));
		} else if(party.equalsIgnoreCase("server")) {
			fileInputStream = new FileInputStream(FileNameGenerator.SIGNATURE_FOR_SERVER);
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PUBLIC_KEY_FOR_SERVER));
		} else {
			throw new Exception("Party name mismatched. \n");
		}
		publicKey = (PublicKey) objInputStream.readObject();
		objInputStream.close();
	}
	
	private static void verifySignatureWithMessage(Signature signature, String message) throws Exception {
		signature.initVerify(publicKey);
		signature.update(message.getBytes());
		byte[] raw = new byte[fileInputStream.available()];
		fileInputStream.read(raw);
		fileInputStream.close();
		System.out.println("	Original Message: " + message);
		System.out.println("	Signed message needs to verified: " + new String(raw));
		if(signature.verify(raw)) {
			System.out.println("	Verify signature successfully.");
			result = true;
		} else {
			System.out.println("	Verify signature failed.");
		}
	}
	
	public static boolean verifySignedMessage(Signature signature, String signedMessage, String encryptedMessage, String party) {
		System.out.println("	--- Verify Signature Message Process Started ---");
		try {
			initializePublicKey(party);
			verifyMessage(signature, signedMessage, encryptedMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("	--- Verify Signature Message Process End ---\n");
		return result;
	}
	
	private static void initializePublicKey(String party) throws Exception {
		if(party.equalsIgnoreCase("client")) {
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PUBLIC_KEY_FOR_CLIENT));
		} else if(party.equalsIgnoreCase("server")) {
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PUBLIC_KEY_FOR_SERVER));
		} else {
			throw new Exception("Party name mismatched. \n");
		}
		publicKey = (PublicKey) objInputStream.readObject();
		objInputStream.close();
	}
	
	private static void verifyMessage(Signature signature, String signedMessage, String encryptedMessage) throws Exception {
		signature.initVerify(publicKey);
		signature.update(encryptedMessage.getBytes());
		byte[] raw = Base64.decodeBase64(signedMessage);
		System.out.println("	Encrypted Message: " + encryptedMessage);
		System.out.println("	Signed message needs to verified: " + signedMessage);
		if(signature.verify(raw)) {
			System.out.println("	Verify signature successfully.");
			result = true;
		} else {
			System.out.println("	Verify signature failed.");
		}
	}
}
