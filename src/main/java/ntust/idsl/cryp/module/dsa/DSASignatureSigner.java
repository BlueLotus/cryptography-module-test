package ntust.idsl.cryp.module.dsa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

import org.apache.commons.codec.binary.Base64;

import ntust.idsl.cryp.util.FileNameGenerator;

/**
 * @author Carl Adler (C.A.)
 * */
public class DSASignatureSigner {
	
	private static FileOutputStream fileOutputStream = null;
	private static ObjectInputStream objInputStream = null;
	private static PrivateKey privateKey;
	
	public static void signSignatureFile(Signature signature, String message, String party) {
		System.out.println("	--- Sign Signature File Process Started ---");
		try {
			initializeFileResources(party);
			signMessageToSignatureFile(signature, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("	--- Sign Signature File Process Finished ---\n");
	}
	
	private static void initializeFileResources(String party) throws Exception {
		if(party.equalsIgnoreCase("client")) {
			fileOutputStream = new FileOutputStream(new File(FileNameGenerator.SIGNATRUE_FOR_CLIENT));
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PRIVATE_KEY_FOR_CLIENT));
		} else if(party.equalsIgnoreCase("server")) {
			fileOutputStream = new FileOutputStream(new File(FileNameGenerator.SIGNATURE_FOR_SERVER));
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PRIVATE_KEY_FOR_SERVER));
		} else {
			throw new Exception("Party name mismatched. \n");
		}
		privateKey = (PrivateKey)objInputStream.readObject();
		objInputStream.close();
	}
	
	private static void signMessageToSignatureFile(Signature signature, String message) throws Exception {
		System.out.println("	Original message: " + message);
		signature.initSign(privateKey);
		signature.update(message.getBytes());
		byte[] raw = signature.sign();
		System.out.println("	Message after signed: " + new String(raw));
		fileOutputStream.write(raw);
		fileOutputStream.close();
	}
	
	public static String signSignatureMessage(Signature signature, String encryptedMessage, String party) {
		System.out.println("	--- Sign Signature Message Process Started ---");
		String signedMessage = null;
		try {
			initializePrivateKey(party);
			signedMessage = signEncryptedMessage(signature, encryptedMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("	--- Sign Signature Message Process Finished ---\n");
		return signedMessage;
	}
	
	private static void initializePrivateKey(String party) throws Exception {
		if(party.equalsIgnoreCase("client")) {
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PRIVATE_KEY_FOR_CLIENT));
		} else if(party.equalsIgnoreCase("server")) {
			objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PRIVATE_KEY_FOR_SERVER));
		} else {
			throw new Exception("Party name mismatched. \n");
		}
		privateKey = (PrivateKey)objInputStream.readObject();
		objInputStream.close();
	}
	
	private static String signEncryptedMessage(Signature signature, String encryptedMessage) throws Exception {
		System.out.println("	Encrypted message: " + encryptedMessage);
		signature.initSign(privateKey);
		signature.update(encryptedMessage.getBytes());
		byte[] raw = signature.sign();
		String resultMsg = Base64.encodeBase64String(raw);
		System.out.println("	Message after signed: " + resultMsg);
		return resultMsg;
	}
}
